// This shopping list program isn't compiling!
// Use your knowledge of generics to fix it.


fn main() {
    // Both <_> and <&str> work

    let mut shopping_list: Vec<_> = Vec::new();
    shopping_list.push("milk");
}

