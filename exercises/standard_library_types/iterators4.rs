// iterators4.rs


pub fn factorial(num: u64) -> u64 {
    // Complete this function to return the factorial of num
    // Do not use:
    // - return
    // Try not to use:
    // - imperative style loops (for, while)
    // - additional variables
    // For an extra challenge, don't use:
    // - recursion
    // Execute `rustlings hint iterators4` for hints.
    
    // if num == 0 {
    //     1
    // }
    // else {
    //     num * factorial(num -1)
    // }
    
    // 5! = 5 * 4 * 3 * 2 * 1
    // 1, 2, 3, 4, 5
    //
    // elem acc x res
    //      1
    // 1    1   1 1
    // 2    1   2 2
    // 3    2   3 6
    // 4    6   4 24
    // 5    24  5 120

    // println!("{:?}", (1..num));
    (1..num + 1).fold(1, |acc, x| acc * x)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
//    fn factorial_of_0() {
//        assert_eq!(1, factorial(0));
//    }
//    #[test]
//    fn factorial_of_1() {
//        assert_eq!(1, factorial(1));
//    }
    #[test]
    fn factorial_of_2() {
        assert_eq!(2, factorial(2));
    }
    #[test]
    fn factorial_of_3() {
        assert_eq!(6, factorial(3));
    }
    #[test]
    fn factorial_of_4() {
        assert_eq!(24, factorial(4));
    }
    #[test]
    fn factorial_of_5() {
        assert_eq!(120, factorial(5));
    }
}
