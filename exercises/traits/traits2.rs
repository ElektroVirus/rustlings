// traits2.rs
//
// Your task is to implement the trait
// `AppendBar' for a vector of strings.
//
// To implement this trait, consider for
// a moment what it means to 'append "Bar"'
// to a vector of strings.
//
// No boiler plate code this time,
// you can do this!

trait AppendBar {
    fn append_bar(self) -> Self;
}

//TODO: Add your code here
impl AppendBar for Vec<String> {
    // This will append "Bar" to each string separately. Not what was asked for :D
    // fn append_bar(mut self) -> Self {
    //     let borrowed_bar: &str = "Bar";
    //     self.iter_mut().for_each(|x| x.to_owned().push_str(borrowed_bar));
    //     return self
    // }
    
    fn append_bar(mut self) -> Self {
        self.push(String::from("Bar"));
        return self;
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn is_vec_pop_eq_bar() {
        let mut foo = vec![String::from("Foo")].append_bar();
        assert_eq!(foo.pop().unwrap(), String::from("Bar"));
        assert_eq!(foo.pop().unwrap(), String::from("Foo"));
    }
}
